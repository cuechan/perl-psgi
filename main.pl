#!/usr/bin/env perl

use strict;
use warnings;


# we are creating a so called 'sub function' which is basically a normal fuction
# we store the function in a variable so plackup finds it, when it executes this file
#
# the function is executed everytime a request is coming in.

my $app = sub {
	# this is useful for some advance usage
	# the $psgi object stores the clients ip address and 
	# other information about the client e.g. cookies, Browser, etc.
	my $psgi = Plack::Request->new(shift);

	# we are creating a response
	my $response = "Hello, World! today is ".localtime(time());


	# and pass the response back to plackup
	return [
		'200',                            # http statuscode
		['Content-Type' => 'text/plain'], # some headers whicj can be set
		[$response]                       # the actuall data
	];
};
