First, install Plack (our framework)
```
sudo cpan install Plack
```

execute with:
```
plackup main.pl
```

then go to http://localhost:5000
